

exports.getschoolteachersubjects = function (req, res) {
    var raw = req.params.authdata;
    var decoded = common.decode(raw);
    console.log(raw);
    console.log(decoded);
    var identity = decoded.split(':')[0];
    var password = decoded.split(':')[1];

    db.collection('schoolteachercollection', function (err, collection) {
        collection.find({ username: identity, password: password }).toArray(function (err, result) {
            if (err) {
                res.send({ 'error': 'An error has occurred' });
            } else {
                console.log('Success: ' + JSON.stringify(result));
                var teacherdetails = processteacher(result);
                res.send(teacherdetails);
            }
        });
    });


};
exports.gettodayteachersubjecttest = function (req, res) {
    var todaytestsetup = [];
    var dateadded = common.gettodaydate();
    var raw = req.params.authdata;
    var selectedlevel = req.params.selectedlevel;
    var selectedsubject = req.params.selectedsubject;
    var level = selectedlevel.replace('-', '/');
    var decoded = common.decode(raw);

    var identity = decoded.split(':')[0];
    var password = decoded.split(':')[1];

    db.collection('schoolteachercollection', function (err, collection) {
        collection.find({ username: identity, password: password }).toArray(function (err, teacherresult) {
            if (err) {
                res.send('teacher not found!!');
            } else if (teacherresult[0] != '' && typeof (teacherresult[0] != 'undefined')) {

                db.collection('teachertodaysubjecttestsetupcollection', function (err, collection) {
                    collection.find({ teacherid: new ObjectID(teacherresult[0]._id), level: level, subject: selectedsubject, dateadded: dateadded }).toArray(function (err, result) {
                        if (err) {
                            res.send({ 'error': 'An error has occurred' });
                        } else {

                            for (var o = 0; o < result.length; o++) {
                                console.log("retrieving");
                                var attend = result[o].attendance;
                                var iden = result[o]._id;
                                var studentid = result[o].studentid;
                                var fullname = result[o].fullname;
                                var dob = result[o].dateofbirth;
                                var teacherid = result[o].teacherid;
                                var level = result[o].level;
                                var subject = result[o].subject;
                                var selected = result[o].selected;
                                var dateadded = result[o].dateadded;
                                var topicid = result[o].topicid;

                                console.log(result[o].topicid);

                                todaytestsetup.push({
                                    studentid: studentid,
                                    identity: iden,
                                    fullname: fullname,
                                    dateofbirth: dob,
                                    teacherid: teacherid,
                                    level: level,
                                    subject: subject,
                                    selected: selected,
                                    dateadded: dateadded,
                                    topicid: topicid
                                });

                            }
                           
                            res.send(todaytestsetup);

                        }
                    });
                })
            }
        })
    })
}
exports.getteacherstudentsubjectattendance = function (req, res) {
    var teacherstudentsubjectattendance = [];
    var dateadded = common.gettodaydate();
    var raw = req.params.authdata;
    var selectedlevel = req.params.selectedlevel;
    var selectedsubject = req.params.selectedsubject;
    var level = selectedlevel.replace('-', '/');
    var decoded = common.decode(raw);

    var identity = decoded.split(':')[0];
    var password = decoded.split(':')[1];
    console.log(level);
    console.log(selectedsubject);
    db.collection('schoolteachercollection', function (err, collection) {
        collection.find({ username: identity, password: password }).toArray(function (err, teacherresult) {
            if (err) {
                res.send('teacher not found!!');
            } else if (teacherresult[0] != '' && typeof (teacherresult[0] != 'undefined')) {
                console.log(teacherresult[0]._id);
                db.collection('teachersubjectstudentattendancecollection', function (err, collection) {
                    collection.find({ teacherid: new ObjectID(teacherresult[0]._id), level: level, subject: selectedsubject, dateadded: dateadded }).toArray(function (err, result) {
                        if (err) {
                            res.send({ 'error': 'An error has occurred' });
                        } else {

                            for (var o = 0; o < result.length; o++) {
                                var attend = result[o].attendance;
                                var iden = result[o]._id;
                                var studentid = result[o].studentid;
                                var fullname = result[o].fullname;
                                var dob = result[o].dateofbirth;
                                var teacherid = result[o].teacherid;
                                var level = result[o].level;
                                var subject = result[o].subject;
                                var selected = result[o].selected;
                                var dateadded = result[o].dateadded;

                                teacherstudentsubjectattendance.push({
                                    studentid: studentid, identity: iden, fullname: fullname,
                                    dateofbirth: dob, teacherid: teacherid, level: level, subject: subject, selected: selected, dateadded: dateadded
                                });

                            }
                            res.send(teacherstudentsubjectattendance);
                        }


                    });
                })
            }
        })
    })
}
exports.getschoolstudentsbysubjectandlevel = function (req, res) {
    var schoolteacherstudent = {
        schoolstudent: [],
        teacherid: '',
        teacherstudents: []
    };
    var raw = req.params.authdata;
    var selectedlevel = req.params.selectedlevel;
    var selectedsubject = req.params.selectedsubject;
    var level = selectedlevel.replace('-', '/');
    var decoded = common.decode(raw);
    console.log(raw);
    console.log(decoded);
    var identity = decoded.split(':')[0];
    var password = decoded.split(':')[1];

    db.collection('schoolteachercollection', function (err, collection) {
        collection.find({ username: identity, password: password }).toArray(function (err, teacherresult) {
            if (err) {
                res.send('teacher not found!!');
            } else if (teacherresult[0] != '' && typeof (teacherresult[0] != 'undefined')) {

                db.collection('schoolstudentcollection', function (err, collection) {
                    collection.find({
                        schoolid: new ObjectID(teacherresult[0].schoolid),
                        'schoolstudent.selectedlevel': level
                    }).toArray(function (err, result) {
                        if (err) {
                            res.send({ 'error': 'An error has occurred' });
                        } else {

                            schoolteacherstudent.schoolstudent = processstudent(result, selectedsubject);
                            schoolteacherstudent.teacherid = teacherresult[0]._id;
                            db.collection('schoolteacherstudentregistrationcollection', function (err, collection) {
                                collection.find({ teacherid: new ObjectID(teacherresult[0]._id), level: level }).toArray(function (err, result) {
                                    if (err) {
                                        res.send({ 'error': 'An error has occurred' });
                                    } else {
                                        if (typeof (result[0]) != 'undefined' && typeof (result[0].students) != 'undefined') {
                                            console.log(teacherresult[0]._id);
                                            console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
                                            console.log(result[0].students);
                                            schoolteacherstudent.teacherstudents = result[0].students;
                                            console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
                                            console.log('Success: ' + JSON.stringify(schoolteacherstudent));
                                        }
                                        res.send(schoolteacherstudent);

                                    }
                                });
                            })


                        }
                    });
                })
            }
        })
    })
}
exports.upadateregistersubjectlevelstudents = function (req, res) {
    var raw = req.params.authdata;
    var dateadded = common.gettodaydate();
    var schoolteachersubjectstudent = req.body;
    var teacherstudents = schoolteachersubjectstudent.teacherstudents;
    var teacherstudentid = schoolteachersubjectstudent.teacherstudentid;
    var selectedlevel = schoolteachersubjectstudent.selectedlevel;
    var decoded = common.decode(raw);

    var identity = decoded.split(':')[0];
    var password = decoded.split(':')[1];

    db.collection('schoolteachercollection', function (err, collection) {
        collection.find({ username: identity, password: password }).toArray(function (err, result) {
            if (err) {
                res.send('teacher not found!!');
            } else if (result[0] != '' && typeof (result[0] != 'undefined')) {
                console.log(result[0].schoolid);
                var schoolid = result[0].schoolid;
                var teacherid = result[0]._id;
                var students = teacherstudents;

                db.collection('schoolteacherstudentregistrationcollection', function (err, collection) {
                    collection.update({ '_id': new ObjectID(teacherstudentid) }, {
                        $set: {
                            schoolid: schoolid,
                            teacherid: teacherid,
                            level: selectedlevel,
                            students: students,
                            dateadded: dateadded
                        }

                    }, { safe: true }, function (err, items) {
                        if (err) {
                            res.send({ 'error': 'An error has occurred' });
                        } else {
                            console.log('teacher student registration updated successfully');
                            res.send(items);
                        }
                    });

                })
            }
        })
    })
}

exports.addschoolstudentsbysubjectandlevel = function (req, res) {
    var raw = req.params.authdata;
    var dateadded = common.gettodaydate();
    var schoolteachersubjectstudent = req.body;
    var teacherstudentsubjects = schoolteachersubjectstudent.teacherstudentsubjects;
    var selectedlevel = schoolteachersubjectstudent.selectedlevel;
    var selectedsubject = schoolteachersubjectstudent.selectedsubject;
    var decoded = common.decode(raw);

    console.log(teacherstudentsubjects);

    var identity = decoded.split(':')[0];
    var password = decoded.split(':')[1];

    db.collection('schoolteachercollection', function (err, collection) {
        collection.find({ username: identity, password: password }).toArray(function (err, result) {
            if (err) {
                res.send('teacher not found!!');
            } else if (result[0] != '' && typeof (result[0]) != 'undefined') {
                console.log(result[0].schoolid);
                var schoolid = result[0].schoolid;
                var teacherid = result[0]._id;


                db.collection('schoolteacherstudentregistrationcollection', function (err, collection) {
                    if (err) {
                        res.send({ 'error': 'An error has occurred' });
                    } else {
                        for (var i = 0; i < teacherstudentsubjects.length; i++) {
                            var itemarray = teacherstudentsubjects[i];
                            var studentid = itemarray.studentid;
                            var subject = selectedsubject;
                            var updateableid = itemarray.identity;
                            var fullname = itemarray.fullname;
                            var dateofbirth = itemarray.dateofbirth;
                            var selected = itemarray.selected;

                            console.log(updateableid);
                            console.log(studentid);
                            console.log(subject);
                            if (typeof (updateableid) == 'undefined' || updateableid == '') {
                                collection.insert({
                                    schoolid: schoolid,
                                    teacherid: teacherid,
                                    level: selectedlevel,
                                    studentid: studentid,
                                    subject: subject,
                                    fullname: fullname,
                                    dateofbirth: dateofbirth,
                                    selected: selected,
                                    dateadded: dateadded

                                }, function () { });
                            }
                            else {
                                collection.update({ '_id': new ObjectID(updateableid) }, {
                                    $set: {
                                        selected: selected,
                                        dateadded: dateadded
                                    }

                                });
                                console.log('updated');
                            }
                        }
                        res.send('passed');
                    }

                })
            }

        })

    });

};
exports.addtodayteachersubjecttest = function (req, res) {
    var raw = req.params.authdata;

    var dateadded = common.gettodaydate();
    var todaytestsetup = req.body;
    var topicid = todaytestsetup.topicid;
    var subject = todaytestsetup.subject;
    var level = todaytestsetup.level;
    var attendance = todaytestsetup.attendance;
    var decoded = common.decode(raw);
    var identity = decoded.split(':')[0];
    var password = decoded.split(':')[1];

    db.collection('schoolteachercollection', function (err, collection) {
        collection.find({ username: identity, password: password }).toArray(function (err, result) {
            if (err) {
                res.send('teacher not found!!');
            } else if (result[0] != '' && typeof (result[0]) != 'undefined') {

                var teacherid = result[0]._id;
                db.collection('teachertodaysubjecttestsetupcollection', function (err, coll) {
                    if (err) {
                        res.send({ 'error': 'An error has occurred' });
                    } else {

                        for (var i = 0; i < attendance.length; i++) {

                            var itemarray = attendance[i];
                            if (typeof (itemarray) != 'undefined' || itemarray != null) {

                                var dateofbirth = itemarray.dateofbirth;
                                var fullname = itemarray.fullname;
                                var studentid = itemarray.studentid;
                                var selected = itemarray.selected;

                                coll.insert({
                                    teacherid: teacherid,
                                    level: level,
                                    subject: subject,
                                    topicid: topicid,
                                    dateofbirth: dateofbirth,
                                    fullname: fullname,
                                    studentid: studentid,
                                    selected: selected,
                                    dateadded: dateadded

                                }, function () { });


                            }
                        }
                        res.send('passed');
                    }

                })
            }
        })
    })
}
exports.gettodaytesttopics = function (req, res) {
    var raw = req.params.authdata;
    var subject = req.params.subject;

    var dateadded = common.gettodaydate();
    var level = req.params.level.replace('-', '/');
    console.log(level);
    console.log(subject);
    var decoded = common.decode(raw);
    var identity = decoded.split(':')[0];
    var password = decoded.split(':')[1];

    db.collection('schoolteachercollection', function (err, collection) {
        collection.find({ username: identity, password: password }).toArray(function (err, result) {
            if (err) {
                res.send('teacher not found!!');
            } else if (result[0] != '' && typeof (result[0]) != 'undefined') {
                var teacherid = result[0]._id;
                console.log(teacherid);
                db.collection('todayteachertesttopicscollection', function (err, tcollection) {
                    tcollection.find({
                        teacherid: new ObjectID(teacherid),
                        level: level,
                        subject: subject,
                        dateadded: dateadded
                    }).toArray(function (err, output) {
                        res.send(output);
                    })
                })
            }
        })

    })
}
exports.gettodaytesttopicsafter = function (req, res) {
    var raw = req.params.authdata;
    var topid = req.params.topid;

    var dateadded = common.gettodaydate();

    var decoded = common.decode(raw);
    var identity = decoded.split(':')[0];
    var password = decoded.split(':')[1];

    db.collection('schoolteachercollection', function (err, collection) {
        collection.find({ username: identity, password: password }).toArray(function (err, result) {
            if (err) {
                res.send('teacher not found!!');
            } else if (result[0] != '' && typeof (result[0]) != 'undefined') {
                var teacherid = result[0]._id;
                console.log(teacherid);
                db.collection('todayteachertesttopicscollection', function (err, tcollection) {
                    tcollection.find({
                        '_id': new ObjectID(topid),
                        teacherid: new ObjectID(teacherid),
                        dateadded: dateadded
                    }).toArray(function (err, output) {
                        res.send(output);
                    })
                })
            }
        })

    })
}
exports.addtodaytesttopics = function (req, res) {
    var raw = req.params.authdata;
    var dateadded = common.gettodaydate();
    var top = req.body;
    var topics = top.topics;

    var decoded = common.decode(raw);
    var identity = decoded.split(':')[0];
    var password = decoded.split(':')[1];

    db.collection('schoolteachercollection', function (err, collection) {
        collection.find({ username: identity, password: password }).toArray(function (err, result) {
            if (err) {
                res.send('teacher not found!!');
            } else if (result[0] != '' && typeof (result[0]) != 'undefined') {
                var teacherid = result[0]._id;
                console.log(teacherid);
                db.collection('todayteachertesttopicscollection', function (err, tcollection) {
                    tcollection.remove({teacherid: teacherid, level: topics[0].level,
                            subject: topics[0].subject, dateadded: dateadded })
                    console.log("Data removed");
                    tcollection.insert(
                        {
                            teacherid: teacherid,
                            topics: topics,
                            level: topics[0].level,
                            subject: topics[0].subject,
                            dateadded: dateadded


                        }, { safe: true }, function (err, result) {
                            if (err) {

                            } else {
                                res.send(JSON.stringify(result.insertedIds[0]));
                            }

                        });
                })
            }
        })

    })
}
exports.addteacherstudentsubjectattendance = function (req, res) {
    var raw = req.params.authdata;
    var dateadded = common.gettodaydate();
    var teachersubjectattendance = req.body;
    var todayattendance = teachersubjectattendance.todayattendance;
    var selectedsubject = teachersubjectattendance.selsubject;
    var selectedlevel = teachersubjectattendance.selectedlevel;
    var decoded = common.decode(raw);
    var identity = decoded.split(':')[0];
    var password = decoded.split(':')[1];

    db.collection('schoolteachercollection', function (err, collection) {
        collection.find({ username: identity, password: password }).toArray(function (err, result) {
            if (err) {
                res.send('teacher not found!!');
            } else if (result[0] != '' && typeof (result[0]) != 'undefined') {
                var teacherid = result[0]._id;
                db.collection('teachersubjectstudentattendancecollection', function (err, collection) {
                    if (err) {
                        res.send({ 'error': 'An error has occurred' });
                    } else {
                        console.log(todayattendance.length);
                        for (var i = 0; i < todayattendance.length; i++) {

                            var itemarray = todayattendance[i];
                            if (typeof (itemarray) != 'undefined' || itemarray != null) {

                                var dateofbirth = itemarray.dateofbirth;
                                var fullname = itemarray.fullname;
                                var studentid = itemarray.studentid;
                                var iden = itemarray.identity;
                                var selected = itemarray.selected;
                                console.log(itemarray);

                                if (typeof (iden) == 'undefined' || iden == '') {
                                    collection.insert({
                                        teacherid: teacherid,
                                        level: selectedlevel,
                                        subject: selectedsubject,
                                        dateofbirth: dateofbirth,
                                        fullname: fullname,
                                        studentid: studentid,
                                        selected: selected,
                                        dateadded: dateadded

                                    }, function () { });
                                }
                                else {
                                    collection.remove({ '_id': new ObjectID(iden) }, function () { });
                                    console.log('item removed');
                                    itemarray.selected = itemarray.selected;
                                    collection.insert({
                                        teacherid: teacherid,
                                        level: selectedlevel,
                                        subject: selectedsubject,
                                        dateofbirth: dateofbirth,
                                        fullname: fullname,
                                        studentid: studentid,
                                        selected: selected,
                                        dateadded: dateadded

                                    }, function () { });

                                }
                            }
                        }
                        res.send('passed');
                    }

                })



            }

        })

    });

};

exports.processstudent = processstudent;
exports.processteacher = processteacher;
exports.getschoolteacherstudentsubjectregistration = function (req, res) {
    var raw = req.params.authdata;
    var selectedlevel = req.params.selectedlevel;
    var selectedsubject = req.params.selectedsubject;

    var level = selectedlevel.replace('-', '/');

    console.log(raw);
    console.log(selectedlevel);
    console.log(selectedsubject);

    var decoded = common.decode(raw);

    var identity = decoded.split(':')[0];
    var password = decoded.split(':')[1];
    db.collection('schoolteachercollection', function (err, collection) {
        collection.find({ username: identity, password: password }).toArray(function (err, result) {

            if (err) {
                res.send('teacher not found!!');
            } else if (result[0] != '' && typeof (result[0]) != 'undefined') {
                var teacherid = result[0]._id;
                db.collection('schoolteacherstudentregistrationcollection', function (err, collection) {
                    collection.find({
                        teacherid: new ObjectID(teacherid),
                        level: level, subject: selectedsubject
                    }).toArray(function (err, resout) {
                        if (err) {
                            res.send({ 'error': 'An error has occurred' });
                        } else {
                            console.log('teacher subject students inserted successfully');
                            console.log(resout);
                            res.send(JSON.stringify(resout));

                        }
                    });
                })
            }
        })
    })
};


function processstudent(items, selectedsubject) {
    var outdata = [];
    for (var i = 0; i < items.length; i++) {
        var id = items[i]._id;
        var schoolstudent = items[i].schoolstudent;
        console.log(schoolstudent);
        console.log(schoolstudent.studentregisteredsubjects);
        console.log(selectedsubject);
        var include = processtoinclude(schoolstudent.studentregisteredsubjects, selectedsubject);
        console.log(include);
        if (include) {
            outdata.push({ id: id, schoolstudent: schoolstudent });
            console.log(outdata);
        }
    }
    return outdata;
}
function processteacher(items) {
    var outdata = [];
    for (var i = 0; i < items.length; i++) {
        var subjects = items[i].schoolteacher.teachersubjects;
        var levels = items[i].schoolteacher.teacherlevels;
        outdata.push({ subjects: subjects, levels: levels });
    }

    return outdata;
}
function processtoinclude(studentsubjects, selectedsubject) {
    var isvalid = false;
    var allsubjects = JSON.parse(studentsubjects);
    for (var i = 0; i < allsubjects.length; i++) {
        console.log(allsubjects[i]);
        if (allsubjects[i] == selectedsubject) {
            isvalid = true;
        }
    }
    return isvalid;
}



