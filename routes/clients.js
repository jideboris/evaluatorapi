
exports.deleteclientstudent = function (req, res) {
  var id = req.params.id;
  var raw = req.params.authdata;
  var decoded = common.decode(raw);
  console.log(raw);
  console.log(decoded);
  var identity = decoded.split(':')[0];
  var password = decoded.split(':')[1];
  db.collection('schoolcollection', function (err, collection) {
    collection.find({ regnumber: identity, schoolpasscode: password }).toArray(function (err, items) {
      if (items != '') {
        var clientid = items[0]._id
        console.log(clientid);
        db.collection('schoolstudentcollection', function (err, collection) {
          collection.remove({ '_id': new ObjectID(id) }, { safe: true }, function (err, result) {
            if (err) {
              res.send({ 'error': 'An error has occurred - ' + err });
            } else {
              console.log('' + result + ' schoolstudentcollection(s) deleted');
              res.send('success');
            }
          });
        });
      }
    });
  });

};
exports.deleteclientteacher = function (req, res) {
  var id = req.params.id;
   console.log(id);
  var raw = req.params.authdata;
  var decoded = common.decode(raw);
  console.log(raw);
  console.log(decoded);
  var identity = decoded.split(':')[0];
  var password = decoded.split(':')[1];
  db.collection('schoolcollection', function (err, collection) {
    collection.find({ regnumber: identity, schoolpasscode: password }).toArray(function (err, items) {
      if (items != '') {
        var clientid = items[0]._id
        console.log(clientid);
        db.collection('schoolteachercollection', function (err, collection) {
          collection.remove({ '_id': new ObjectID(id) }, { safe: true }, function (err, result) {
            if (err) {
              res.send({ 'error': 'An error has occurred - ' + err });
            } else {
              console.log('' + result + ' schoolteachercollection(s) deleted');
              res.send('success');
            }
          });
        });
      }
    });
  });

}
exports.updateclientstudent = function (req, res) {
  var id = req.params.id;
  var schoolstudent = req.body;
  console.log(schoolstudent);
  var raw = req.params.authdata;
  var decoded = common.decode(raw);
  console.log(raw);
  console.log(decoded);
  var identity = decoded.split(':')[0];
  var password = decoded.split(':')[1];
  db.collection('schoolcollection', function (err, collection) {
    collection.find({ regnumber: identity, schoolpasscode: password }).toArray(function (err, items) {
      if (items != '') {
        var clientid = items[0]._id
        console.log(clientid);
        db.collection('schoolstudentcollection', function (err, collection) {
          collection.update({ '_id': new ObjectID(id) }, {
            $set: {
              schoolstudent: schoolstudent,
              schoolid: clientid
            }
          }, { safe: true }, function (err, result) {
            if (err) {
              console.log('Error updating schoolstudentcollection: ' + err);
              res.send({ 'error': 'An error has occurred' });
            } else {
              console.log('passed ');
              res.send(result);

            }
          });
        });
      }
    });
  });

}
exports.updateclientteacher = function (req, res) {
  var id = req.params.id;
  var dateadded = common.gettodaydate();
  var schoolteacher = req.body;
  console.log(schoolteacher);
  var raw = req.params.authdata;
  var decoded = common.decode(raw);
  console.log(raw);
  console.log(decoded);
  var identity = decoded.split(':')[0];
  var password = decoded.split(':')[1];
  db.collection('schoolcollection', function (err, collection) {
    collection.find({ regnumber: identity, schoolpasscode: password }).toArray(function (err, items) {
      if (items != '') {
        var clientid = items[0]._id
        console.log(clientid);
        db.collection('schoolteachercollection', function (err, collection) {
          collection.update({ '_id': new ObjectID(id) }, {
            $set: {
              schoolteacher: schoolteacher,
              schoolid: clientid,
              dateadded: dateadded
            }
          }, { safe: true }, function (err, result) {
            if (err) {
              console.log('Error updating schoolteachercollection: ' + err);
              res.send({ 'error': 'An error has occurred' });
            } else {
              console.log('passed ');
              res.send(result);

            }
          });
        });
      }
    });
  });

}
exports.addteacherschoolclient = function (req, res) {
  var dateadded = common.gettodaydate();
  var schoolteacher = req.body;
  console.log(schoolteacher);
  var raw = req.params.authdata;
  var decoded = common.decode(raw);
  console.log(raw);
  console.log(decoded);
  var identity = decoded.split(':')[0];
  var password = decoded.split(':')[1];
  var username = schoolteacher.fullname.replace(/ /g, "");
  var item = username + schoolteacher.dateofbirth + schoolteacher.personalemail;
  var teacherpassword = common.makeid(item.toUpperCase());
  db.collection('schoolcollection', function (err, collection) {
    collection.find({ regnumber: identity, schoolpasscode: password }).toArray(function (err, items) {
      if (items != '') {
        var clientid = items[0]._id
        console.log(clientid);
        db.collection('schoolteachercollection', function (err, collection) {
          collection.insert({
            schoolteacher: schoolteacher,
            schoolid: clientid,
            username: username,
            password: teacherpassword,
            dateadded: dateadded

          }, { safe: true }, function (err, result) {
            if (err) {
              res.send({ 'error': 'An error has occurred' });
            } else {
              console.log('teacher inserted successfully');
              res.send(JSON.stringify(result));
            }
          });
        });
      }
    });
  });

};

exports.addclientstudent = function (req, res) {
  var dateadded = common.gettodaydate();
  var schoolstudent = req.body;
  console.log(schoolstudent);
  var raw = req.params.authdata;
  var decoded = common.decode(raw);
  console.log(raw);
  console.log(decoded);
  var identity = decoded.split(':')[0];
  var password = decoded.split(':')[1];
  var username = schoolstudent.fullname.replace(/ /g, "");
  var item = username + schoolstudent.dateofbirth + schoolstudent.personalemail;
  var studentpassword = common.makeid(item.toUpperCase());
  db.collection('schoolcollection', function (err, collection) {
    collection.find({ regnumber: identity, schoolpasscode: password }).toArray(function (err, items) {
      if (items != '') {
        var clientid = items[0]._id
        console.log(clientid);
        db.collection('schoolstudentcollection', function (err, collection) {
          collection.insert({
            schoolstudent: schoolstudent,
            schoolid: clientid,
            username: username,
            password: studentpassword,
            dateadded: dateadded

          }, { safe: true }, function (err, result) {
            if (err) {
              res.send({ 'error': 'An error has occurred' });
            } else {
              console.log('student inserted successfully');
              res.send(JSON.stringify(result));
            }
          });
        });
      }
    });
  });

};
exports.getteacherstudentsby = function (req, res) {
  var raw = req.params.authdata;
  var id = req.params.id;
  var decoded = common.decode(raw);
  console.log(raw);
  console.log(decoded);
  var identity = decoded.split(':')[0];
  var password = decoded.split(':')[1];
  db.collection('schoolcollection', function (err, collection) {
    collection.find({ regnumber: identity, schoolpasscode: password }).toArray(function (err, items) {
      if (items != '') {
        var clientid = items[0]._id
        console.log(clientid);
        db.collection('schoolteachercollection', function (err, collection) {
          collection.find({ schoolid: new ObjectID(clientid), '_id': new ObjectID(id) }).toArray(function (err, result) {
            if (err) {
              res.send({ 'error': 'An error has occurred' });
            } else {
              console.log('Success: ' + JSON.stringify(result));
              res.send(result);
            }
          });
        });
      }
      else {
        res.send('client not found!');
      }
    });
  });

};
exports.getschoolstudentsby = function (req, res) {
  var raw = req.params.authdata;
  var id = req.params.id;
  var decoded = common.decode(raw);
  console.log(raw);
  console.log(decoded);
  var identity = decoded.split(':')[0];
  var password = decoded.split(':')[1];
  db.collection('schoolcollection', function (err, collection) {
    collection.find({ regnumber: identity, schoolpasscode: password }).toArray(function (err, items) {
      if (items != '') {
        var clientid = items[0]._id
        console.log(clientid);
        db.collection('schoolstudentcollection', function (err, collection) {
          collection.find({ schoolid: new ObjectID(clientid), '_id': new ObjectID(id) }).toArray(function (err, result) {
            if (err) {
              res.send({ 'error': 'An error has occurred' });
            } else {
              console.log('Success: ' + JSON.stringify(result));
              res.send(result);
            }
          });
        });
      }
      else {
        res.send('client not found!');
      }


    });
  });

};
exports.getschoolteachers = function (req, res) {
  var raw = req.params.authdata;
  var decoded = common.decode(raw);
  console.log(raw);
  console.log(decoded);
  var identity = decoded.split(':')[0];
  var password = decoded.split(':')[1];
  db.collection('schoolcollection', function (err, collection) {
    collection.find({ regnumber: identity, schoolpasscode: password }).toArray(function (err, items) {
      if (items != '') {
        var clientid = items[0]._id
        console.log(clientid);
        db.collection('schoolteachercollection', function (err, collection) {
          collection.find({ schoolid: new ObjectID(clientid) }).toArray(function (err, result) {
            if (err) {
              res.send({ 'error': 'An error has occurred' });
            } else {
              console.log('Success: ' + JSON.stringify(result));
              res.send(result);
            }
          });
        });
      }
      else {
        res.send('client not found!');
      }


    });
  });

};
exports.getschoolstudents = function (req, res) {
  var raw = req.params.authdata;
  var decoded = common.decode(raw);
  console.log(raw);
  console.log(decoded);
  var identity = decoded.split(':')[0];
  var password = decoded.split(':')[1];
  db.collection('schoolcollection', function (err, collection) {
    collection.find({ regnumber: identity, schoolpasscode: password }).toArray(function (err, items) {
      if (items != '') {
        var clientid = items[0]._id
        console.log(clientid);
        db.collection('schoolstudentcollection', function (err, collection) {
          collection.find({ schoolid: new ObjectID(clientid) }).toArray(function (err, result) {
            if (err) {
              res.send({ 'error': 'An error has occurred' });
            } else {
              console.log('Success: ' + JSON.stringify(result));
              res.send(result);
            }
          });
        });
      }
      else {
        res.send('client not found!');
      }


    });
  });

};

