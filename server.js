var express = require('express');
path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
mime = require('mime');

mongo = require('mongodb');
fs = require('fs');
formidable = require('formidable');
// path = require('path');
mailer = require("nodemailer");
Mailgun = require('mailgun-js');
mg = require('nodemailer-mailgun-transport');
nodemailer = require('nodemailer');
sendgrid = require('sendgrid')('jideboris', 'computer123');
EmailTemplates = require('email-templates').EmailTemplate;

Server = mongo.Server,
    Db = mongo.Db,
    //  BSON = mongo.BSONPure;
    BSON = mongo.BSON;
ObjectID = mongo.ObjectID;

server = new Server('localhost', 27017, { auto_reconnect: true });
db = new Db('evaluatordb', server);

db.open(function (err, db) {
    if (!err) {
        console.log("Connected to 'evaluatordb' database");
    }
    else {
        console.log("The 'evaluatordb' collection doesn't exist. Creating it with sample data...");
    }
});


var edata = require('./routes/edata');
var equestion = require('./routes/questions');
var clients = require('./routes/clients');
var adminclients = require('./routes/adminclients');
var teachers = require('./routes/teachers');
common = require('./routes/common');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

app.engine('html', require('ejs').renderFile);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');
app.set('view engine', 'html');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
 
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, X-Codingpedia,Authorization");
    res.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS");
    next();
});

app.use(express.static(path.join(__dirname, 'public')));




app.get('/topics/:level1/:level2/', edata.findAllTopics);
app.get('/topicsbysubject/:selectedsubject', edata.findAllSubjectTopics);
app.post('/addtopic', edata.addtopic);
app.get('/topicbyid/:id', edata.findtopicbyid);
app.put('/updatetopic/:id', edata.updatetopic);
app.delete('/deletetopic/:id', edata.deletetopic);
app.get('/locations', edata.findAllLocations);
app.post('/addlocation', edata.addlocation);
app.delete('/deletelocation/:id', edata.deletelocation);
app.get('/locationbyid/:id', edata.findLocationById);
app.put('/updatelocation/:id', edata.updatelocation);
app.get('/subjects/:level1/:level2/:level3', edata.findAllSubjects);
app.post('/addsubject', edata.addsubject);
app.get('/subjectbyid/:id', edata.findsubjectbyid);
app.put('/updatesubject/:id', edata.updatesubject);
app.delete('/deletesubject/:id', edata.deletesubject);

app.get('/admins', edata.findAllAdmins);
app.post('/addadmin', edata.addadmin);
app.delete('/deleteadmin/:id', edata.deleteadmin);
app.get('/adminbyid/:id', edata.findAdminById);
app.put('/updateadmin/:id', edata.updateadmin);


app.get('/questioncount', equestion.questioncount);
app.get('/questions', equestion.findAllQuestions);
app.post('/addlackedskillset', equestion.addlackedskillsets);
app.post('/fileUpload', equestion.uploadAvatar);
app.post('/updatefileuploaded', equestion.updatefileuploaded);
app.post('/addquestion', equestion.addquestion);
app.get('/lackedskillset', equestion.findLackedSkillSets);
app.delete('/deletelackedskillset/:id', equestion.deletelackedskillset);
app.delete('/deletequestion/:id', equestion.deletequestion);
app.get('/questionbyid/:id', equestion.findquestionbyid);
app.put('/updatequestion/:id', equestion.updatequestion);
app.get("/questionavatarbyid/:id/", equestion.findquestionavatarbyid);


app.delete('/deleteschool/:id', adminclients.deleteschool);
app.get('/schoolbyid/:id', adminclients.findschoolbyid);
app.post('/addschool', adminclients.addschool);
app.post('/updateschool', adminclients.updateschool);
app.get('/addschoolaccount/:mail/:passcode/:schoolname/:username/:license', adminclients.addschoolaccount);
app.get('/schools/:chklocked', adminclients.findschool);
app.get('/allschools', adminclients.findallschool);
app.get('/allschoolsbylock/:locked', adminclients.findalllockedschool);
app.put('/updateaccount/:id/:updatedlocked/:lock', adminclients.updateaccount);
app.get('/clientidentity/:category/:identity', adminclients.clientidentity);
app.get('/submit/:mail/:passcode/:schoolname/:username/:license', common.sendemail);


app.post('/addclientstudent/:authdata', clients.addclientstudent);
app.put('/updateclientstudent/:id/:authdata', clients.updateclientstudent);
app.delete('/deleteclientstudent/:id/:authdata', clients.deleteclientstudent);
app.delete('/deleteclientteacher/:id/:authdata', clients.deleteclientteacher);
app.get('/schoolstudents/:authdata', clients.getschoolstudents);
app.get('/schoolstudentsby/:id/:authdata', clients.getschoolstudentsby);
app.post('/uploadbatchclientstudent', common.excelbatchprocessing);
app.post('/addteacherschoolclient/:authdata', clients.addteacherschoolclient);
app.put('/updateclientteacher/:id/:authdata', clients.updateclientteacher);
app.get('/schoolteachers/:authdata', clients.getschoolteachers);
app.get('/schoolteachersby/:id/:authdata', clients.getteacherstudentsby);

//teachers
app.get('/schoolteachersubjects/:authdata', teachers.getschoolteachersubjects);
app.get('/schoolstudentsbysubjectandlevel/:selectedlevel/:selectedsubject/:authdata', teachers.getschoolstudentsbysubjectandlevel);
app.post('/addschoolstudentsbysubjectandlevel/:authdata', teachers.addschoolstudentsbysubjectandlevel);
app.post('/upadateregistersubjectlevelstudents/:authdata', teachers.upadateregistersubjectlevelstudents);
app.post('/addteacherstudentsubjectattendance/:authdata', teachers.addteacherstudentsubjectattendance);
app.get('/teacherstudentsubjectattendance/:selectedlevel/:selectedsubject/:authdata', teachers.getteacherstudentsubjectattendance);
app.get('/schoolteacherstudentsubjectregistration/:selectedsubject/:selectedlevel/:authdata', teachers.getschoolteacherstudentsubjectregistration);
app.post('/todayteachersubjecttest/:authdata', teachers.addtodayteachersubjecttest);

app.get('/retrievetodaytesttopics/:subject/:level/:authdata', teachers.gettodaytesttopics);

app.get('/retrievetodaytesttopicsafter/:topid/:authdata', teachers.gettodaytesttopicsafter);

app.post('/todaytesttopics/:authdata', teachers.addtodaytesttopics);

app.get('/retrievetodayteachersubjecttest/:selectedlevel/:selectedsubject/:authdata', teachers.gettodayteachersubjecttest);

app.listen(3000);
console.log('Listening on port 3000...');
